require 'stool/Core/Config'
require 'version'
module Stool
  class Command < CLAide::Command

    #配置中心
    @@config = Config::loadConfig

    require 'stool/Command/LibPool'
    require 'stool/Command/WorkSpace'
    require 'stool/Command/Lib'
    require 'stool/Command/Repo'
    require 'stool/Command/Util'

    self.abstract_command = true
    self.command = 'sl'
    self.version = VERSION
    self.description = <<-Detail
      smart tool - 对iOS相关工作，进行自动化整合.
      RubyGem:https://rubygems.org/gems/stool
      Git:https://gitee.com/CWDong/stool
    Detail
    self.plugin_prefixes = %w(claide stool)

    def self.options
      [
        ['--silent', 'Show nothing'],
      ].concat(super)
    end

    def initialize(argv)
      checkConfigFile
      super
    end

    def run
      puts 'it works!!'

      cc = Config::loadConfig

      cc.pools.map do |x|

        puts x.path
        puts x.name
      end

    end

    def pp(*args)
        if args.inspect[1]
          puts args[0]
        end
    end

    #检测系统Config文件
    def checkConfigFile

    end

  end
end
