#本地pod lib项目
require 'Core/LibInfo'
require 'Command/Lib/VersionAdd'
require 'Command/LibPool/List'

module Stool
  class Command
    class Lib < Command

      # self.abstract_command = true
      self.command = 'lib'
      self.summary = '本地lib信息、处理'
      # self.default_subcommand = 'list'
      self.arguments = [
          CLAide::Argument.new('NAME',   true),
      ]

      #要查询的lib名字
      attr_accessor :name
      #查询结果lib信息
      attr_accessor :info

      def initialize(argv)
        # @info = LibInfo.new()
        @name = argv.shift_argument
        # @progress = argv.flag?('progress')
        super
      end

      def validate!
        super
        unless @name
          help! 'need the lib `NAME`.'
        end

        unless existAtPools?
          help! "the lib named #{@name} did not find."
        end
      end

      def run
        if @info
          @info.info_tos
        end
      end

      def existAtPools?
        #在libPool中查找lib
        arrayPools = LibPool::List::pools_from_config
        arrayPools.each {|pool|
          Dir.new(pool.path).each{|file|
            if @name.eql?(file)
              path = pool.path + "/#{file}"
              @info = LibInfo::loadFromPath(path)
            end
          }
        }
        @info
      end
        
    end
  end
end
