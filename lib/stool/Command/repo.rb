#私有库的池，池中有多个lib
require 'Core/RepoInfo'

module Stool
  class Command
    class Repo < Command

      require 'Command/Repo/CacheClean'

      self.abstract_command = true
      self.command = 'rp'
      self.summary = 'cocoapods的repos信息，及相关操作'
      # self.default_subcommand = 'list'

      def initialize(argv)
        super
      end

      def run

      end

      def self.cacheCleanAll
        puts "clean all cache"
        hasClean = []
        path = @@config.repoCachePath
        Dir.entries(path).each{|file|
          if file['.'] || file.eql?('master')
            #do nothing
          else
            podCache = File.join(path,file)
            Dir.entries(podCache).sort.each{|podName|
              unless hasClean.include?(podName) || podName['.']
                puts "清理缓存#{podName} "
                puts `pod cache clean #{podName} --all`
                hasClean.push(podName)
              end
            }
          end
        }
      end

      #获取所有repo除master
      def self.getAllRepos
        path = @@config.repoCachePath
        arr = Dir.entries(path).delete_if{|file|
          if file['.'] || file.eql?('master')
            1
          end
        }
      end

    end
  end
end
