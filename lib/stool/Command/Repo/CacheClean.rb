#查看当前pool列表
module Stool
  class Command
    class Repo < Command
      class CacheClean < Repo

        self.command = 'cc'
        self.summary = '清除pod所有相关缓存'

        # self.arguments = [
        #     CLAide::Argument.new('NAME',   false),
        # ]

        def initialize(argv)
          super
        end

        #查看当前的pool list
        def run
          CacheClean::cacheCleanAll
        end
        @public
      end
    end
  end
end
