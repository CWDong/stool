#私有库的池，池中有多个lib

require 'active_support/core_ext/string/strip'
require 'Core/PoolInfo'

module Stool
  class Command
    class LibPool < Command

      require 'Command/LibPool/List'

      self.abstract_command = true
      self.command = 'lp'
      self.summary = 'LibPool - 本地pod池.'
      self.default_subcommand = 'list'

      #pool信息
      # attr_accessor :pInfo

      def initialize(argv)
        super
      end

      #配置文件中读取
      def self.pools_from_config
        arr = @@config.pools
        arr.each do |obj|
          # puts '**** obj===' + "#{obj}"
          if obj.respond_to?('path')
            unless File.exist?(obj.path)
              puts "warning__> #{obj.path} not exist"
              arr.delete(obj)
            end
          end
        end
        arr
      end

      #检测、git clone
      def self.existLocalAndClone(libs)

      end

    end
  end
end
