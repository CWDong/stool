
module Stool
  class Command
    class Util < Command
      class Jenkins < Util

        self.command = 'jk'
        self.summary = 'Jenkins启动、关闭'
        self.arguments = [
            CLAide::Argument.new('close', false),
        ]

        def initialize(argv)
          @close = argv.shift_argument
          super
        end

        def run
          if @close
            `sudo launchctl unload /Library/LaunchDaemons/org.jenkins-ci.plist`
          else
            `sudo launchctl load /Library/LaunchDaemons/org.jenkins-ci.plist`
          end
        end

      end
    end
  end
end
