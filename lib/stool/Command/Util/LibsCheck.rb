require 'Core/LibInfo'
require "spreadsheet"
require 'colored2'

module Stool
  class Command
    class Util < Command
      class LibsCheck < Util

        self.command = 'lc'
        self.summary = 'libsCheck - 统计repos中，所有pod库的信息，如：版本、简介、git地址'
        # self.arguments = [
        #     CLAide::Argument.new('close', false),
        # ]

        def initialize(argv)
          if @@config.checkRepos.count > 0
            Spreadsheet.client_encoding="utf-8"
            #创建表格对象
            @book = Spreadsheet::Workbook.new
          end
          super
        end

        def run
          @@config.checkRepos.map { |e|
            summary_repo e
          }
          #在指定路径下面创建test1.xls表格，并写book对象
          path = File.join(Config.logsDirPath,'pod_summary.xls')
          Dir.chdir(Config.logsDirPath)
          @book.write path

          puts "已经生成Excel文件<#{path}>"

          `open #{path}`
        end

        #统计某一个pod库
        def summary_repo(repo)
          repoPath = File.join(Dir.home,'.cocoapods/repos',repo)

          unless Dir.exist?(repoPath)
            puts "#{repoPath} does not exist!!".red
            puts "Now,we have found some repos,which did you mean: ".red
            puts `pod repo`.white
            raise
          end

          puts "更新当前repo - #{repo}".white
          puts `pod repo update #{repo}`

          puts "生成表格中...".white
          #创建工作表
          sheet1 = @book.create_worksheet :name => "pod_summary_" + repo
          #在表格第一行设置分类
          sheet1.row(0)[0]="名字"
          sheet1.row(0)[1]="简介"
          sheet1.row(0)[2]="所属业务模块"
          sheet1.row(0)[3]="主页"
          sheet1.row(0)[4]="统计时版本"
          Dir.chdir(repoPath)

          libs = []
          Dir.entries(repoPath).sort.uniq.each do |dir|
            unless dir['.']
              #取出最大版本
              arr = Dir.entries(File.join(repoPath,dir)).sort_by do |x|
                v_to_i x
              end

              if arr.last
                fname = Dir.entries(File.join(repoPath,dir,arr.last)).last
                libinfo = LibInfo.loadFromPath(File.join(repoPath,dir,arr.last))
                if libinfo
                  libs.push(libinfo)
                end
              end
            end
          end

          puts Dir.entries(repoPath).count

          mod = ''
          libs.each_with_index {|lib,idx|
            mod = '其他工具'
            @@config.checkReposSheets.each do|dic|
              dic.values[0].each do |pod|
                if lib.name.downcase.eql?(pod.downcase)
                  mod = dic.keys[0]
                  break
                end
              end
            end

            sheet1.row(idx+1)[0]= lib.name
            sheet1.row(idx+1)[1]= lib.summary
            sheet1.row(idx+1)[2]= mod
            sheet1.row(idx+1)[3]= lib.homepage
            sheet1.row(idx+1)[4]= lib.version
          }
        end

        def v_to_i (x)
          maxX = 10
          a = 0
          arr = x.split('.').reverse
          arr.map.with_index {|x, idx|
            a = a + x.to_i * 10**(idx * maxX)
          }
          a
        end

      end
    end
  end
end
