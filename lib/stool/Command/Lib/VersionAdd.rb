#版本号
require 'Utils/StringUtil'
include StringUtile

module Stool
  class Command
    class Lib < Command
      class VersionAdd < Lib

        self.command = 'vadd'
        self.summary = '本地lib版本号增加'
        self.arguments = [
            CLAide::Argument.new('NAME',   true),
        ]

        def run
          @info.info_tos
          puts '版本增加 1'
          newVersion = StringUtile::versionUpdate(@info.version,'1')
          file = File.open(@info.path + "/#{@info.name}" + ".podspec", "r+")
            file.each_line do |line|
              if line[/#{@info.version}/]
                file.seek(-line.length, IO::SEEK_CUR)
                file.write(line.sub(/#{@info.version}/, newVersion))
              end
            end
          file.close
        end
        
      end
    end
  end
end
