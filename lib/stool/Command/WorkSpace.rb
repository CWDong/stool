#iOS-workspcace项目处理
require 'Core/TaskInfo'
require 'Command/Repo'

module Stool
  class Command
    class WorkSpace < Command

      require 'Command/WorkSpace/list'
      require 'Command/WorkSpace/update'

      self.abstract_command = true
      self.command = 'ws'
      self.summary = 'workSpaces - 本地的Workspace项目，关于pod相关的整合'
      # self.default_subcommand = 'list'

      #要查询的workspace的唯一标识
      attr_accessor :tag
      #查询结果workspace信息
      attr_accessor :info

      def initialize(argv)
        super
      end

      @public

      def validate!
        super
      end

      # def run
      #
      # end

      def hasPodfile?(path)
        arr = Dir.glob(File.join(path,'Podfile'))
        arr[0]
      end

      def existAtConfig?
        #在本地配置中中查找workspace
        arrayTasks = WorkSpace::List::tasks_from_config
        arrayTasks.each {|task|
          if @tag.eql?(task.tag)
            @info = task
          end
        }
        @info
      end

      #获取task用的所有repos
      def getAllRepos
        arr = []
        if @info.repos.count == 0
          arr = Repo::getAllRepos
        else
          arr = @info.repos
        end
        arr
      end

      def pathForTaskInfo_last_pod_update
        unless Dir.exist?(Config::logsDirPath)
          Dir.mkdir(Config::logsDirPath)
        end
        File.join(Config::logsDirPath, 'TaskInfo_lastUpdate.yaml')
      end

      #清理所有pod缓存（除master repo的pod）
      def cleanAllCache
        Repo::cacheCleanAll
      end

    end
  end
end
