
module Stool
  class Command
    class Util < Command

      require 'Command/Util/Jenkins'
      require 'Command/Util/libsCheck'
      self.abstract_command = true
      self.command = 'util'
      self.summary = '常用工具的集合'

      def initialize(argv)
        super
      end

    end
  end
end
