#查看当前workspace列表

module Stool
  class Command
    class WorkSpace < Command
      class List < WorkSpace

        self.command = 'list'
        self.summary = 'workspace列表'

        self.arguments = [
            #CLAide::Argument.new('NAME',   false),
        ]

        def initialize(argv)
          super
        end

        #查看当前的pool list
        def run
          pp('')
          pp('**WorkSpace list')
          List::tasks_from_config.map{|wp|
            wp.info_tos
          }
        end

        @public

        def self.tasks_from_config
          arr = @@config.tasks
          arr.map do |obj|
            if obj.respond_to?('path')
              unless File.exist?(obj.path)
                puts "warning--> #{obj.path} not exist"
                arr.delete(obj)
              end
            end
          end
          arr
        end

      end
    end
  end
end
