#对执行pod update的整合
require 'Utils/ArrayUtile'

module Stool
  class Command
    class WorkSpace < Command
      class Update < WorkSpace

        self.command = 'up'
        self.summary = '移除pods、podfile.lock后,执行pod update'

        self.arguments = [
            CLAide::Argument.new('Tag',   true),
        ]

        def initialize(argv)
          @tag = argv.shift_argument
          super
        end

        def validate!
          unless @tag
            help! 'need the tag of workspace you configed.'
          end
          unless existAtConfig?
            help! "To run, please ensure the tag(#{@tag}) exist."
          end
          unless hasPodfile?(@info.path)
            help! 'To run, please ensure the workspace contain a podfile in zhe folder.' + "#{@info.path}"
          end
          super
        end

        @public

        def run
          repos_up
          localLibs_git_up
          @info.task_git_up
          @info.edit_podfile_use_loaclLibs
          del_folers
          check_lastUpdate
          pod_update
          @info.resetPofile
          @info.open_task
        end

        #更新相关repo(除了master)
        def repos_up
          getAllRepos.each do |repo|
            puts "*** begin update repo <#{repo}>"
            `pod repo update #{repo}`
            puts "*** end update repo"
            puts ''
          end
        end

        #更新项目用到的所有本地库
        def localLibs_git_up
          @@config.pools.map do |pool|
            @info.localLibs.map do |lib|
              libInfo = pool.searchLib(lib)
              if libInfo
                libInfo.gitUp
              end
            end
          end
        end

        #删除一些相关文件
        def del_folers
          Dir.chdir(@info.path)
          # Dir.rmdir(File.join(@info.path, 'Pods'))
          puts `rm -rf Pods/`

          podLock = File.join(@info.path,"Podfile.lock")
          if File.exist?(podLock)
            File.delete(podLock)
          end
        end

        include ArrayUtile
        #检测上一次更新用到的lib，源码是否与本次有冲突
        def check_lastUpdate
          lastTaskInfo = TaskInfo::fromYAML(pathForTaskInfo_last_pod_update)
          unless lastTaskInfo
            cleanAllCache
            return
          end

          if lastTaskInfo.allUseCode != @info.allUseCode
            cleanAllCache
            return
          end

          diff = diffOf(lastTaskInfo.otherCode,@info.otherCode)
          diff.each do |x|
            puts "--- 清除lib缓存:#{x} ..."
            puts `pod cache clean #{x} --all`
          end
        end

        #执行pod update
        def pod_update
          useCode = @info.localLibs + @info.otherCode
          useCode = useCode.uniq
          useCode.collect! {|item|
            item + "_use_code=1 "
          }
puts useCode
          libs_useCode = useCode.join
          if @info.allUseCode
            libs_useCode = 'use_code=1' + libs_useCode
          end

          @info.doCacheByYAML(pathForTaskInfo_last_pod_update)

          puts "---源码库有" + libs_useCode
          Dir.chdir(@info.path)
          puts '---当前目录' + Dir.getwd
          puts "---update中...."
          puts `#{libs_useCode} pod update --no-repo-update`
        end

      end
    end
  end
end
