#查看当前pool列表

module Stool
  class Command
    class LibPool < Command
      class Clone < LibPool

        self.command = 'cl'
        self.summary = '本地pod池中clone'

        self.arguments = [
            CLAide::Argument.new('NAME',   false),
        ]

        def initialize(argv)
          @poolName
          super
        end

        #查看当前的pool list
        def run
          pp('')
          pp('**clone to pool')
          List::pools_from_config.map do |pInfo|
            pInfo.info_tos
          end

        end

        @public

      end
    end
  end
end
