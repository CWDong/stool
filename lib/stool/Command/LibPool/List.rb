#查看当前pool列表
module Stool
  class Command
    class LibPool < Command
      class List < LibPool

        self.command = 'list'
        self.summary = '本地pod池列表'

        self.arguments = [
            CLAide::Argument.new('NAME',   false),
        ]

        def initialize(argv)
          super
        end

        #查看当前的pool list
        def run
          pp('')
          pp('**Pool list')
          List::pools_from_config.map do |pInfo|
            pInfo.info_tos
          end
        end

        @public

      end
    end
  end
end
