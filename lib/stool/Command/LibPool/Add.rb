#添加一个池
module Stool
  class Command
    class LibPool < Command
      class Add < LibPool

        self.command = 'add'
        self.summary = '添加一个本地pod池.'

        self.description = <<-DESC
          '添加一个私有pod库的池，'
        DESC

        self.arguments = [
            CLAide::Argument.new('NAME',   true),
            CLAide::Argument.new('PATH',   true),
        ]

        # def self.options
        #   [
        #       ['--progress', 'Show the progress of cloning the spec repository'],
        #   ].concat(super)
        # end

        def initialize(argv)
          puts 'Add ---  init'
          @pInfo.name = argv.shift_argument
          @pInfo.path = argv.shift_argument
          # @progress = argv.flag?('progress')
          super
        end

        def validate!
          super
          puts 'Add ---  validate!'
          unless @pInfo.name && @pInfo.path
            help! 'Adding a libPool needs a `NAME` and a `PATH`.'
          end

          unless File.exist?(@path)
            raise Informative,
                  'To add a pool, please give a ensure the path of folder is exist.'
          end
        end

        def run
          puts 'Add ---  run'
          #do cache
          obj = hadCached?
          if obj
            obj.path = @path
          else
            doCacheByYamlToPath(@@PathForCache,@name)
          end

          # section = "Cloning spec repo `#{@name}` from `#{@url}`"
          # section << " (branch `#{@branch}`)" if @branch
          # UI.section(section) do
          #   create_repos_dir
          #   clone_repo
          #   checkout_branch
          #   config.sources_manager.sources([dir.basename.to_s]).each(&:verify_compatibility!)
          # end
        end

        def hadCached?
          puts "@PathForCache = #{@@PathForCache}"
          puts "@name = #{@name}"

          obj = nil
          if File.exist?(self.cachePath)
            obj = YAML::load(File.open(self.cachePath))
          end
        end

        def doCacheByYamlToPath(cacheDirPath,fileName)
          puts "....do cache to #{cacheDirPath}"
          unless File::exists?(cacheDirPath)
            Dir.mkdir(cacheDirPath,0777)
          end
          File.open(File.join(cacheDirPath,fileName) + '.yaml', "w") { |file|
            YAML.dump(self, file)
          }
        end

        def cachePath
          path = "#{@@PathForCache + @name + '.yaml'}"
          path.to_s
        end
      end
    end
  end
end
