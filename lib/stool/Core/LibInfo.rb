require 'Core/BaseObj'
require 'Core/Git'

module Pod
  class String
    class << self
      def strip_heredoc

      end
      def method_missing(method_id, *arguments, &block)
        # puts "***" + "method_id = #{method_id}"
      end
    end
  end

  class Ios
    def method_missing(method_id, *arguments, &block)
      # puts "***" + "method_id = #{method_id}"
    end
  end

  class Osx
    def method_missing(method_id, *arguments, &block)
      # puts "***" + "method_id = #{method_id}"
    end
  end

  class Tvos
    def method_missing(method_id, *arguments, &block)
      # puts "***" + "method_id = #{method_id}"
    end
  end

  class Watchos
    def method_missing(method_id, *arguments, &block)
      # puts "***" + "method_id = #{method_id}"
    end
  end

  class Spec
    attr_accessor :name
    attr_accessor :version
    attr_accessor :ios
    attr_accessor :osx
    attr_accessor :tvos
    attr_accessor :watchos
    #简介
    attr_accessor :summary
    attr_accessor :homepage
    # attr_accessor :name
    # attr_accessor :name

    def method_missing(method_id, *arguments, &block)
      # puts "***" + "method_id = #{method_id}"
    end

    @public

    def initialize
      @ios = Ios.new()
      @osx = Osx.new()
      @tvos = Tvos.new()
      @watchos = Watchos.new()
      yield self if block_given?
    end

    def self.fromFile(path)
      content = File.open(path, 'r:utf-8', &:read)
      eval(content,nil,path.to_s)
    end

  end
end

#######################################

module Stool
  class LibInfo < BaseObj

    include Stool::Git_util

    #本地路径
    attr_accessor :path
    #lib的name
    attr_accessor :name
    #lib的版本
    attr_accessor :version
    #本地缓存类型（Framework or code）
    attr_accessor :cacheType
    #简介
    attr_accessor :summary
    attr_accessor :homepage

    @public

    def self.loadFromPath (path)
      Dir.chdir(path)
      podspec = Dir.glob('*.podspec')[0]
      if podspec
        info = LibInfo.new()
        info.path = path
        info.name = podspec.split('.')[0]
        info.dsl_podspec(path + "/#{podspec}")
      end

      info
    end

    def info_tos
      puts ''
      puts "**lib info of <#{self.name}>"
      puts "-- name: #{self.name}"
      puts "-- path: #{self.path}"
      puts "-- version: #{self.version}"
      puts ''
    end

    def dsl_podspec(path)
      spec = Pod::Spec.fromFile(path)
      @name = spec.name
      @version = spec.version
      @summary = spec.summary
      @homepage = spec.homepage
    end

    def gitUp
      if @path
        Dir.chdir(@path)
        puts "*** git处理<#{@name}>"
        git_st
        result = git_stash
        git_up
        if result['Saved working']
          git_stash_pop
        else
          puts "no stash to pop"
        end
      end
    end

  end
end
