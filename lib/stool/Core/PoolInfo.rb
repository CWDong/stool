#本地lib池

require 'Core/BaseObj'
require 'Core/LibInfo'

module Stool
  class PoolInfo < BaseObj
    attr_accessor :name
    attr_accessor :path
    attr_accessor :lib_gitCloneUrl

    def initialize
      @lib_gitCloneUrl = ''
      super
    end

    def info_tos
      puts('--' + 'name:'+ self.name)
      puts('--' + 'path:'+ self.path)
      puts('--' + 'path:'+ self.lib_gitCloneUrl.to_s)
      puts('--' + 'libs:')
      subDirs.sort.each do |f|
        unless f['.']
          puts '  ' + f.to_s
        end
      end
      puts('')
    end

    #本地搜索一个lib
    def searchLib(name)
      libInfo = nil
      if subDirs.include?(name)
        libInfo = LibInfo::loadFromPath(File.join(@path,name))
      end
      libInfo
    end

    def subDirs
      arr = []
      if @path
        arr = Dir.entries(@path)
      end
      arr
    end

  end
end
