#本地lib池
require 'Core/BaseObj'
require 'Core/Git'
require 'Core/Config'

module Stool
  class TaskInfo < BaseObj

    include Stool::Git_util

    attr_accessor :name
    attr_accessor :path
    attr_accessor :tag

    #pod时，指定的本地lib name
    attr_accessor :localLibs
    #其他源码指定
    attr_accessor :otherCode
    attr_accessor :allUseCode

    #指定的repo
    attr_accessor :repos

    def initialize
      @localLibs = []
      @otherCode = []
      @repos = []
      @allUseCode = false
      super
    end

    def info_tos
      puts('--' + 'name:'+ self.name)
      puts('--' + 'tag:'+ self.tag)
      puts('--' + 'path:'+ self.path)
      puts('--' + 'localLibs:')
      self.localLibs.sort.map do |lib|
        puts('  ' + lib.to_s)
      end
      puts('--' + 'otherCode:')
      self.otherCode.sort.map do |lib|
        puts('  ' + lib.to_s)
      end
      puts('--' + 'allUseCode:'+ self.allUseCode.to_s)
      puts('')
    end

    def addLocalLib(libs)
      libs.each do |lib|
        @localLibs.push(lib)
      end
    end

    def addOtherLibUseCode(libs)
      libs.each do |lib|
        @otherCode.push(lib)
      end
    end

    def allLibsUseCode
      @allUseCode = true
    end

    ########################

    def open_task
      `open *.xcworkspace`
    end

    def resetPofile
      git_checkout(File.join(@path,'Podfile'))
    end

    #更新主工程
    def task_git_up
      if @path
        Dir.chdir(path)
        git_up
      end
    end

    #更新podfile，添加使用本地库
    def edit_podfile_use_loaclLibs
      podfilePath = File.join(@path,'Podfile')

      if File.exist?(podfilePath)
        arr = @localLibs
        newStr = ''
        lastMatch = ''
        File.open(podfilePath, "r+") do |f|
          f.each_line do |line|
            lineNew = line
            hasFound = []
            arr.each do |libName|
              regx = "pod '#{libName}'"
              if line[/#{regx}/]
                config = Config.loadConfig
                libInfo = nil
                config.pools.each do |pool|
                  result = pool.searchLib(libName)
                  if result
                    libInfo = result
                  end
                end
                if libInfo
                  lineNew = regx + ", :path => '#{libInfo.path}'" + "\n"
                  lastMatch = lineNew
                  hasFound.push(libName)
                end
              end
            end
            arr = arr - hasFound
            newStr = newStr + lineNew
          end
        end

        File.open(podfilePath, "w") do |file|
          # puts newStr
          file.write(newStr)
        end

        if arr.count > 0
          newStr = ''
          File.open(podfilePath, "r+") do |f|
            f.each_line do |line|
              lineNew = line
              regx = "#{lastMatch}"
              if line[/#{regx}/]
                arr.map do |libName|
                  config = Config.loadConfig
                  libInfo = nil
                  config.pools.each do |pool|
                    result = pool.searchLib(libName)
                    if result
                      libInfo = result
                    end
                  end
                  if libInfo
                    lineNew = lineNew + "\n" + "pod '#{libName}'" + ", :path => '#{libInfo.path}'" + "\n"
                  end
                end
              end
              newStr = newStr + lineNew
            end
          end

          File.open(podfilePath, "w") do |file|
            file.write(newStr)
          end
        end
      end
    end

  end
end
