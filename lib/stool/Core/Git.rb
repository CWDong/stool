#update

module Stool
  module Git_util

    #状态
    def git_st
      puts "--- git status"
      `git status`
    end

    def git_stash
      puts "--- git stash"
      `git stash`
    end

    def git_stash_pop
      puts '--- git stash pop'
      `git stash pop`
    end

    #提交
    def git_commit_all(msg)

      `git diff`

      puts "--- 提交所有变更代码"
      `git add .`

      unless msg
        msg = 'stool - auto commit'
      end

      `git commit -m"#{msg}"`
    end

    #update
    def git_up
      puts "--- git update"
      `git pull`
    end

    def git_checkout(file)
      `git checkout #{file}`
    end

  end
end