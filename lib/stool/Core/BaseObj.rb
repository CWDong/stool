
# require 'Object'
require 'yaml'

module Stool
  class BaseObj < Object

    @public

    def initialize
      super
      yield self if block_given?
    end

    def self.fromFile(path)
      if File.exist?(path)
        content = File.open(path, 'r:utf-8', &:read)
        eval(content,nil,path.to_s)
      end
    end

    #加载从yaml
    def self.fromYAML(path)
      if File.exist?(path)
        content = File.open(path)
        YAML.load(content)
      end
    end

    #以yaml缓存
    def doCacheByYAML(path)
      if path
        File.open(path, "w") { |file| YAML.dump(self, file) }
      end
    end

  end
end
