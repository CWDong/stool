#本地lib池
require 'Core/BaseObj'

module Stool
  class RepoInfo < BaseObj
    attr_accessor :name
    attr_accessor :path

    def info_tos
      puts('--' + 'name:'+ self.name)
      puts('--' + 'path:'+ self.path)
      puts('--' + 'Repos:')
      subDirs.sort.each do |f|
        unless f['.']
          puts '  ' + f.to_s
        end
      end
      puts('')
    end

  end
end
