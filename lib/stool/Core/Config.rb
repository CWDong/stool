#项目相关配置
require 'Core/BaseObj'
require 'Core/PoolInfo'
require 'Core/TaskInfo'

module Stool
  class Config < BaseObj

    @public
    #cache相关的路径
    @@cachePath = File.join(Dir.home(),'stool')
    @@configFilePath = File.join(@@cachePath, 'ConfigFile.rb')
    @@logsDirPath = File.join(@@cachePath, 'log')

    #本地lib池子
    attr_accessor :pools
    #主工程
    attr_accessor :tasks
    #cocoapods的repo缓存路径
    attr_accessor :repoCachePath

    #repo pod库的统计
    attr_accessor :checkRepos
    attr_accessor :checkReposSheets

    def initialize
      @repoCachePath = File.join(Dir.home,'.cocoapods/repos')
      @pools = []
      @tasks = []
      @checkRepos = []
      @checkReposSheets = []
      super
    end

    def self.logsDirPath
      @@logsDirPath
    end

    #从配置中心读取
    def self.loadConfig
      checkConfigFile?
      cc = Config::fromFile(@@configFilePath)
      cc
    end

    #添加一个本地libPool
    def addPool
      pInfo = PoolInfo.new()
      @pools.push(pInfo)
      yield pInfo if block_given?
    end

    #添加一个workspace
    def addTask
      tInfo = TaskInfo.new()
      @tasks.push(tInfo)
      yield tInfo if block_given?
    end

    @private

    #检测config.rb是否存在
    def self.checkConfigFile?
      path = File.join(@@configFilePath)
      unless File.exist?(path)
        creatConfigFile
        puts "*** At first ,you should edit the config file at path<#{@@configFilePath}>!!"

        `open #{@@configFilePath}`
      end
    end

    #创建一个ConfigFile例子
    def self.creatConfigFile

      unless Dir.exist?(@@cachePath)
        Dir.mkdir(@@cachePath,0777)
      end

      File.new(@@configFilePath,'w+')

      f = File.open(@@configFilePath, "w+")
      demo = <<-EOF

        #stool 配置

        Config.new do |c|

          #添加一个本地lib pool，池中是你clone下来的各个pod
          #name - 根据爱好，随意取
          #path - lib pool的本地路径
          c.addPool do |p|
            p.name = '本地lib池 - 1'
            p.path = ''
          end

          #再添加一个本地lib pool
          # c.addPool do |p|
          #     p.name = '本地lib池 - 2'
          #     p.path = ''
          # end

          #添加一个项目工程，iOS-workspace工程
          #name - 随意取
          #tag - 命令操作需要，工程的唯一标识
          #path - 路径目下要有workspace/podfile文件
          #addLocalLib - 项目使用的本地lib(再无需手动修改Podfile)，默认都是源码
          #addOtherLibUseCode - 非本地lib，配置使用源码
          #allLibsUseCode - 是否所有lib都使用源码，默认false
          c.addTask do |t|
            t.name = 'workspace主工程-1'
            t.tag = '1'
            t.path = ''
            t.addLocalLib(['pod1',
              'pod2',
              'pod3'])
            t.addOtherLibUseCode(['pod4'])
            #t.allLibsUseCode
          end

          #设置pod库统计时，查询的repos、Excel中sheet的名字

        end
      EOF

      f.write(demo)
    end

  end
end
