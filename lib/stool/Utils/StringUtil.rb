module StringUtile

  #类似version = '3.2.01.01.01'格式的版本更新

  def self.versionUpdate(version,value)

    version = version.to_s

    #要改变的版本
    change = value.to_i

    #更新后版本
    newVersion = (version.gsub('.', '').to_i + change).to_s

    while newVersion.length < version.gsub('.', '').length do
      newVersion = "0"+ newVersion
    end

    #字符串反转

    temV = version.reverse

    temNv = newVersion.reverse

    #新版本号int转string

    appending = '.'

    i = 0

    while i < temV.size

      if temV[i].eql?('.')

        temNv = temNv.insert(i, appending)

      end

      i = i + 1

    end

    puts "#{version}更新版本(#{change})后是#{temNv.reverse}"

    temNv.reverse

  end

end

#examle
# StringUtile::versionUpdate('99.9.99','11')
