module ArrayUtile

  #找到两个数组中，不同的元素
  def diffOf(a,b)
    sum = a + b
    sum.uniq
  end

end
