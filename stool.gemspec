
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'stool/version'

Gem::Specification.new do |spec|
  spec.name          = "stool"
  spec.version       = Stool::VERSION
  spec.authors       = ["Fiend"]
  spec.email         = ["784767574@qq.com"]

  spec.summary       = %q{A tool for workflow!}
  spec.description   = %q{对当前iOS相关操作，进行自动化整合，目前有对Cocoapods的自动化整合!}
  #spec.homepage      = "https://www.jianshu.com/u/b85a12de9c84"#
  spec.homepage      = "https://gitee.com/CWDong/stool"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata["allowed_push_host"] = "https://www.jianshu.com/u/b85a12de9c84"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against " \
  #     "public gem pushes."
  # end

  spec.files         = %w(README.md) + Dir['lib/**/*.rb']
  spec.bindir        = "bin"
  spec.require_paths = %w(lib) + %w(lib/stool)
  spec.executables = %w(sl)

  #third part
  spec.add_runtime_dependency 'activesupport', '>= 4.0.2', '< 5'
  spec.add_runtime_dependency 'atomos',         '~> 0.1.2'
  spec.add_runtime_dependency 'CFPropertyList', '>= 2.3.3', '< 4.0'
  # spec.add_runtime_dependency 'claide',         '>= 1.0.2', '< 2.0'
  spec.add_runtime_dependency 'colored2',       '~> 3.1'
  spec.add_runtime_dependency 'nanaimo',        '~> 0.2.4'

  # spec.add_runtime_dependency 'cocoapods-core'
  spec.add_runtime_dependency 'cocoapods', '~> 1.5.0'
  spec.add_runtime_dependency 'spreadsheet', '~> 1.1.6'

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
