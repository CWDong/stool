#stool 配置
Config.new do |c|

  #添加一个本地lib pool
  c.addPool do |p|
    p.name = '本地lib池 - 凤凰'
    p.path = '/Users/syswin/Documents/Siyuan/Libs/phenix'
  end
  #添加一个本地lib pool
  c.addPool do |p|
      p.name = '本地lib池 - toon基线'
      p.path = '/Users/syswin/Documents/Siyuan/Libs/toon'
  end

  #配置一个主工程
  c.addTask do |t|
    t.name = '凤凰主工程'
    t.tag = '1'
    t.path = '/Users/syswin/Documents/Siyuan/Libs/phenix/tlauncher/TLauncher'
    #默认都是源码
    t.localLibs = ['tcardReader',
      'tcardLibrary',
      'tcardKit',
      'tcontentGroup',
      'tcontentCommon']
  end

end
