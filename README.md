# Stool

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/stool`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'stool'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install stool

## Usage
1、更新stool/ConfigFile.rb \n

    #stool 配置
    Config.new do |c|

      #添加一个本地lib pool，池中是你clone下来的各个pod
      #name - 随意取
      #path - lib pool的本地路径
      c.addPool do |p|
        p.name = '本地lib池 - 1'
        p.path = ''
      end

      #再添加一个本地lib pool
      # c.addPool do |p|
      #     p.name = '本地lib池 - 2'
      #     p.path = ''
      # end

      #添加一个主工程，iOS-workspace工程
      #name - 随意取
      #tag - 命令操作需要，工程的唯一标识
      #path - 路径目下要有workspace/podfile文件
      #addLocalLib - 项目使用的本地lib(再无需手动修改Podfile)，默认都是源码
      #addOtherLibUseCode - 非本地lib，配置使用源码
      #allLibsUseCode - 是否所有lib都使用源码，默认false
      c.addTask do |t|
        t.name = 'workspace主工程-1'
        t.tag = '1'
        t.path = ''
        t.addLocalLib(['pod1',
          'pod2',
          'pod3'])
        t.addOtherLibUseCode(['pod4'])
        #t.allLibsUseCode
      end
    end

2、入口：终端输入sl \n

  Usage:

     $ sl COMMAND

       smart tool - 对iOS相关工作，进行自动化整合。

  Commands:

    + lib       本地lib信息、处理
    + lp        LibPool - 本地pod池.
    + rp        cocoapods的repos信息，及相关操作
    + util      常用工具的集合
    + ws        workSpaces - 本地的Workspace项目，关于pod相关的整合

3、更新libpool中的某个lib版本号

  `sl lib vadd [libName]`

  示例：`sl lib vadd AFNetWorking`

4、主工程workspace执行pod update

  `sl ws up [tag]`

  示例：`sl ws up 1`


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/stool. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Stool project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/stool/blob/master/CODE_OF_CONDUCT.md).
